﻿using System;
using System.Collections.Generic;
using System.Linq;
using InfectionLogic;
using NUnit.Framework;

[assembly: CLSCompliant(false)]

namespace InfectionTests
{
    /// <summary>
    /// Class containing tests for <see cref="Person"/>.
    /// </summary>
    [TestFixture]
    public class InfectionTesting
    {
        /// <summary>
        /// Tests whether calling <see cref="Person(bool)"/> correctly assigns the ID.
        /// </summary>
        /// <param name="number">
        /// The number of times the constructor is called := the ID of the last <see cref="Person"/> object in a collection filled up by consecutively calling <see cref="Person(bool)"/>.
        /// </param>
        [TestCase(28)]
        [TestCase(40)]
        [TestCase(2)]
        public void PersonIDAutoIncrements(int number)
        {
            List<int> personIDs = new(capacity: number);
            List<int> indexers = new(capacity: number);
            Person.ResetCounter();

            for (int i = 0; i < number; i++)
            {
                indexers.Add(i + 1);
                Person newPerson = new(assignID: true);
                personIDs.Add(newPerson.ID);
            }

            Assert.That(personIDs, Is.EqualTo(indexers));
        }

        /// <summary>
        /// Test method to check whether <see cref="Person.MovePerson(int, int, int, int)"/> moves the person off the field.
        /// </summary>
        /// <param name="x">The horizontal (X) component of the motion vector.</param>
        /// <param name="y">The vertical (Y) component of the motion vector.</param>
        /// <param name="width">Maximum allowed horizontal motion := the left edge of the playing filed.</param>
        /// <param name="height">Maximum allowed vertical motion := the bottom edge of the playing filed.</param>
        [TestCase(-17, -17, 9, 9)]
        [TestCase(20, 20, 9, 9)]
        [TestCase(-11, 20, 9, 9)]
        [TestCase(20, -11, 9, 9)]
        public void PersonMovesWithinBoundaries(int x, int y, int width, int height)
        {
            Person person = new(assignID: false);

            person.MovePerson(x, y, width, height);

            Assert.IsTrue(person.X <= width && person.X >= 0);
            Assert.IsTrue(person.Y <= height && person.Y >= 0);
        }

        /// <summary>
        /// Tests whether <see cref="Person.InteractWith(Person)"/> throws <see cref="ArgumentNullException"/> if the parameter is <c>null</c>.
        /// </summary>
        [Test]
        public void PersonInteractsWithNullThrowsException()
        {
            Assert.Throws<ArgumentNullException>(() => new Person(assignID: false).InteractWith(null));
        }

        /// <summary>
        /// Test method for checking the behaviour (and return value) of <see cref="Person.InteractWith(Person)"/>.
        /// </summary>
        /// <param name="thisP">Data for the first person.</param>
        /// <param name="otherP">Data for the second person.</param>
        [TestCase(new int[] { 10, 10, 0, 0 }, new int[] { 8, 12, 0, 0 })] // within range, neither infected, neither masked
        [TestCase(new int[] { 10, 10, 1, 1 }, new int[] { 8, 12, 1, 1 })] // within range, both infected, both masked
        [TestCase(new int[] { 10, 10, 1, 0 }, new int[] { 8, 12, 0, 0 })] // within range, A is infected, neither masked
        [TestCase(new int[] { 10, 10, 0, 0 }, new int[] { 8, 12, 1, 0 })] // within range, B is infected, neither masked
        [TestCase(new int[] { 10, 10, 1, 1 }, new int[] { 8, 12, 0, 0 })] // within range, A is infected, A is masked
        [TestCase(new int[] { 10, 10, 0, 0 }, new int[] { 8, 12, 1, 1 })] // within range, B is infected, B is masked
        [TestCase(new int[] { 10, 10, 1, 0 }, new int[] { 8, 12, 0, 1 })] // within range, A is infected, B is masked
        [TestCase(new int[] { 10, 10, 0, 1 }, new int[] { 8, 12, 1, 0 })] // within range, B is infected, A is masked
        [TestCase(new int[] { 2, 3, 0, 0 }, new int[] { 8, 9, 0, 0 })] // out of range, neither infected, neither masked
        [TestCase(new int[] { 2, 3, 1, 0 }, new int[] { 8, 9, 0, 0 })] // out of range, A is infected, neither masked
        [TestCase(new int[] { 2, 3, 0, 1 }, new int[] { 8, 9, 1, 1 })] // out of range, B is infected, both masked
        public void PersonInteractWithReturnsCorrectly1(int[] thisP, int[] otherP)
        {
            if (thisP is null || otherP is null)
            {
                throw new ArgumentNullException(nameof(thisP) + " or " + nameof(otherP), " was NULL");
            }

            Person p1 = new(x: thisP[0], y: thisP[1], isInfected: thisP[2] == 1, isMasked: thisP[3] == 1);
            Person p2 = new(x: otherP[0], y: otherP[1], isInfected: otherP[2] == 1, isMasked: otherP[3] == 1);
            bool wasP1infected = p1.IsInfected, wasP2infected = p2.IsInfected;

            bool didInteract = p1.InteractWith(p2); // Act

            if (didInteract)
            {
                Assert.IsTrue(p1.IsInfected && p2.IsInfected);
                Assert.That(Math.Sqrt(Math.Pow(p1.X - p2.X, 2) + Math.Pow(p1.Y - p2.Y, 2)), Is.LessThan(3));
            }
            else
            {
                Assert.IsTrue(p1.IsInfected == wasP1infected);
                Assert.IsTrue(p2.IsInfected == wasP2infected);
            }
        }

        /// <summary>
        /// Test method for checking the behaviour (and return value) of <see cref="Person.InteractWith(Person)"/>.
        /// </summary>
        [Test]
        public void PersonInteractWithReturnsCorrectly2()
        {
            Person[] allPeople = GetPeople();
            List<bool[]> actual = new();
            List<bool[]> expected = new();

            for (int i = 0; i < allPeople.Length; i++)
            {
                Person p1 = allPeople[i];
                for (int j = 0; j < i; j++)
                {
                    Person p2 = allPeople[j];
                    bool wasP1infectious = p1.IsInfected, wasP2infectious = p2.IsInfected;
                    actual.Add(new bool[]
                    {
                        Math.Abs(p1.X - p2.X) < 3 && Math.Abs(p1.Y - p2.Y) < 3 && (p1.IsInfected || p2.IsInfected) && !(p1.IsMasked && p2.IsMasked),
                        p1.IsInfected,
                        p2.IsInfected,
                    });
                    bool didInteract = p1.InteractWith(p2);
                    expected.Add(new bool[]
                    {
                        didInteract,
                        didInteract
                            ? (wasP1infectious ? p1.IsInfected == wasP1infectious : p1.IsInfected != wasP1infectious)
                            : p1.IsInfected,
                        didInteract
                            ? (wasP2infectious ? p2.IsInfected == wasP2infectious : p2.IsInfected != wasP2infectious)
                            : p2.IsInfected,
                    }); // if interacted => (T, T, T) else => (F, <no change>, <no change>)
                }
            }

            actual.ForEach(result =>
            {
                if (result[0])
                {
                    result[1] = true;
                    result[2] = true;
                }
            }); // either this or a seperate list is needed for the infected
            Assert.That(actual, Is.EqualTo(expected));
        }

        private static Person[] GetPeople() => new Person[]
        {
            new Person { ID = 1, X = 2, Y = 2, IsInfected = false, IsMasked = false },
            new Person { ID = 2, X = 2, Y = 2, IsInfected = false, IsMasked = true },
            new Person { ID = 3, X = 2, Y = 2, IsInfected = true, IsMasked = false },
            new Person { ID = 4, X = 2, Y = 2, IsInfected = true, IsMasked = true },
            new Person { ID = 5, X = 4, Y = 4, IsInfected = false, IsMasked = false },
            new Person { ID = 6, X = 4, Y = 4, IsInfected = false, IsMasked = true },
            new Person { ID = 7, X = 4, Y = 4, IsInfected = true, IsMasked = false },
            new Person { ID = 8, X = 4, Y = 4, IsInfected = true, IsMasked = true },
            new Person { ID = 9, X = 8, Y = 8, IsInfected = false, IsMasked = false },
            new Person { ID = 10, X = 8, Y = 8, IsInfected = false, IsMasked = true },
            new Person { ID = 11, X = 8, Y = 8, IsInfected = true, IsMasked = false },
            new Person { ID = 12, X = 8, Y = 8, IsInfected = true, IsMasked = true },
        };
    }
}
