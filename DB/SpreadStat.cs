﻿// <copyright file="SpreadStat.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DB
{
    /// <summary>
    /// Data class for storing statistics about the spread of a virus / disease.
    /// </summary>
    public class SpreadStat
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpreadStat"/> class.
        /// </summary>
        /// <param name="count">The number of cases.</param>
        /// <param name="avgX">The average X (horizontal) coordinate of the cases.</param>
        /// <param name="avgY">The average Y (vertical) coordinate of the cases.</param>
        public SpreadStat(int count, double avgX, double avgY)
        {
            this.Count = count;
            this.AvgX = avgX;
            this.AvgY = avgY;
        }

        /// <summary>
        /// Gets or sets the number of cases.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Gets or sets the average X (horizontal) coordinate of the cases.
        /// </summary>
        public double AvgX { get; set; }

        /// <summary>
        /// Gets or sets the average Y (vertical) coordinate of the cases.
        /// </summary>
        public double AvgY { get; set; }

        /// <summary>
        /// Gets or Sets the PK.
        /// </summary>
        public int DummyKey { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"(X= {this.AvgX}, Y= {this.AvgY}) - {this.Count}";
        }
    }
}
