﻿// <copyright file="SpreadDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace DB
{
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// A <see cref="DbContext"/> descendant for the <see cref="SpreadStat"/> database.
    /// </summary>
    public class SpreadDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpreadDbContext"/> class.
        /// </summary>
        public SpreadDbContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets or Sets the set of <see cref="SpreadStat"/> objects.
        /// </summary>
        public virtual DbSet<SpreadStat> SpreadStatSet { get; set; }

        /// <inheritdoc/>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (optionsBuilder is null)
            {
                throw new System.ArgumentNullException(nameof(optionsBuilder), nameof(this.OnConfiguring) + " must not take a null parameter!");
            }

            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseLazyLoadingProxies()
                    .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB; AttachDbFilename=|DataDirectory|\SpreadDb.mdf; Integrated Security=True; MultipleActiveResultSets = true");
            }
        }

        /// <inheritdoc/>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            if (modelBuilder is null)
            {
                throw new System.ArgumentNullException(nameof(modelBuilder), nameof(this.OnModelCreating) + " must not take a null parameter!");
            }

            modelBuilder.Entity<SpreadStat>(entity =>
            {
                entity.HasKey(stat => stat.DummyKey);
                entity.Property(stat => stat.Count).IsRequired();
                entity.Property(stat => stat.AvgX).IsRequired();
                entity.Property(stat => stat.AvgY).IsRequired();
            });
        }
    }
}
