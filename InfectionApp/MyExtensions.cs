﻿// <copyright file="MyExtensions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace InfectionApp
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// Extension class containing extra methods.
    /// </summary>
    public static class MyExtensions
    {
        /// <summary>
        /// Prints the contents of a sequence in the console window.
        /// </summary>
        /// <typeparam name="T">A general type.</typeparam>
        /// <param name="input">The sequence to print.</param>
        /// <param name="title">The title to accompany the printing of <paramref name="input"/>.</param>
        public static void PrintToConsole<T>(this IEnumerable<T> input, string title = "")
        {
            if (input is null)
            {
                Console.WriteLine("Nothing to print!");
                return;
            }

            Console.WriteLine($"\n\t Begin: {title}");
            foreach (T item in input)
            {
                Console.WriteLine(item.ToString());
            }

            Console.WriteLine($"\t END: {title}");
        }
    }
}
