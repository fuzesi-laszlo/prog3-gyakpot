﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace InfectionApp
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading;
    using System.Threading.Tasks;
    using System.Xml.Linq;
    using DataGenerator;
    using DB;
    using InfectionLogic;

    /// <summary>
    /// Class containing the entry point of execution, <see cref="Main"/>.
    /// </summary>
    internal class Program
    {
        private static readonly Random Rnd = new();
        private static readonly object ListLock = new();

        private static void Main()
        {
            CancellationTokenSource cts = new();
            const int additionalPersonCount = 50;
            int w = Console.WindowWidth, h = Console.WindowHeight - 1;
            List<Person> personList = new();
            XDocument xdoc = Generator.GenerateData("QKNWZ1", 6, w, h);
            IEnumerable<XElement> locations = xdoc.Root.Elements("outbreak").Elements("location");

            bool isEven = false;
            foreach (var location in locations)
            {
                isEven = !isEven;
                personList.Add(new Person(
                    x: int.Parse(location.Attribute("x").Value, System.Globalization.NumberFormatInfo.InvariantInfo),
                    y: int.Parse(location.Attribute("y").Value, System.Globalization.NumberFormatInfo.InvariantInfo),
                    isInfected: true,
                    isMasked: isEven));
            }

            for (int i = 0; i < additionalPersonCount; i++)
            {
                isEven = !isEven;
                personList.Add(new Person(x: Rnd.Next(w), y: Rnd.Next(h), isInfected: false, isMasked: isEven));
                /*newPerson.MovePerson(r.Next(), r.Next(), w, h); //instead of assignments in ctor, this will move newPerson from (0,0)*/
            }

            personList.ForEach(person => PersonToConsole(person));
            Console.ReadLine();
            List<SpreadInfo> spreadingList = new();
            new Task(() => PrintNumbers(personList, cts.Token), cts.Token, TaskCreationOptions.LongRunning).Start();
            while (!Console.KeyAvailable)
            {
                Console.Clear();
                lock (ListLock)
                {
                    personList.ForEach(person => person.MovePerson(dx: Rnd.Next(-1, 2), dy: Rnd.Next(-1, 2), w, h));
                    List<Person> healthyPeople = personList.Where(person => !person.IsInfected).ToList();
                    List<Person> infectedPeople = personList.Where(person => person.IsInfected).ToList();
                    infectedPeople.ForEach(infected => healthyPeople.ForEach(healthy =>
                    {
                        if (infected.InteractWith(healthy))
                        {
                            spreadingList.Add(new SpreadInfo(infected.ID, healthy.ID, infected.X, infected.Y));
                        }
                    }));
                    personList.ForEach(person => PersonToConsole(person));
                }

                Thread.Sleep(100);
            }

            cts.Cancel();
            cts.Dispose();
            Console.SetCursorPosition(0, Console.WindowHeight);
            Console.WriteLine();

            spreadingList.PrintToConsole(nameof(spreadingList));
            Console.WriteLine(xdoc);

            /*
            var q7a = spreadingList.GroupBy(spread => spread.SourceID).Select(grp => new
            {
                Count = grp.Count(),
                avgX = grp.Average(spread => spread.X),
                avgY = grp.Average(spread => spread.Y),
            }).OrderByDescending(atyp => atyp.Count);
            */
            var q7a = from spread in spreadingList
                      group spread by spread.SourceID into spreadGrp
                      let spreadData = new
                      {
                          Count = spreadGrp.Count(),
                          avgX = spreadGrp.Average(spread => spread.X),
                          avgY = spreadGrp.Average(spread => spread.Y),
                      }
                      orderby spreadData.Count descending
                      select spreadData;
            /*
            var q7b = spreadingList.Join(xdoc.Root.Elements("outbreak"),
                spread => spread.X,
                outbreak => int.Parse(outbreak.Element("location").Attribute("x").Value, System.Globalization.NumberFormatInfo.InvariantInfo),
                (spread, outbreak) => new
                {
                    spread,
                    outbreakLocY = int.Parse(outbreak.Element("location").Attribute("y").Value, System.Globalization.NumberFormatInfo.InvariantInfo)
                })
                .Where(jointype => jointype.spread.Y == jointype.outbreakLocY)
                .Select(filteredjoin => new
                {
                    locX = filteredjoin.spread.X,
                    locY = filteredjoin.spread.Y,
                    id1_infectedByThisPersonId = filteredjoin.spread.SourceID,
                    id2_personIdThatWasInfected = filteredjoin.spread.TargetID,
                });
            */
            var q7b = from spread in spreadingList
                      join outbreak in xdoc.Root.Elements("outbreak")
                      on new
                      {
                          spread.X,
                          spread.Y,
                      }
                      equals new
                      {
                          X = int.Parse(outbreak.Element("location").Attribute("x").Value, System.Globalization.NumberFormatInfo.InvariantInfo),
                          Y = int.Parse(outbreak.Element("location").Attribute("y").Value, System.Globalization.NumberFormatInfo.InvariantInfo),
                      }
                      select new
                      {
                          locX = spread.X,
                          locY = spread.Y,
                          id1_infectedByThisPersonId = spread.SourceID,
                          id2_personIdThatWasInfected = spread.TargetID,
                      };
            q7a.PrintToConsole(nameof(q7a));
            q7b.PrintToConsole(nameof(q7b));

            SpreadDbContext ctx = new();
            foreach (var item in q7a)
            {
                ctx.SpreadStatSet.Add(new SpreadStat(item.Count, item.avgX, item.avgY));
            }

            ctx.SaveChanges();
            ctx.SpreadStatSet.PrintToConsole(nameof(ctx.SpreadStatSet));
            Console.ReadLine();
            ctx.Dispose();
        }

        private static void PersonToConsole(Person person)
        {
            Console.SetCursorPosition(person.X, person.Y + 1);
            Console.ForegroundColor = person.IsInfected ? ConsoleColor.Red : ConsoleColor.Blue;
            if (person.IsMasked)
            {
                Console.Write('M');
            }
            else
            {
                Console.Write('o');
            }

            Console.ResetColor();
        }

        private static void PrintNumbers(ICollection<Person> people, CancellationToken ct)
        {
            while (!ct.IsCancellationRequested)
            {
                lock (ListLock)
                {
                    Console.SetCursorPosition(0, 0);
                    int countInfected = people.Where(person => person.IsInfected).Count();
                    //// countInfected = (from person in people where person.IsInfected select person).Count();
                    Console.Write($"Already infected: {countInfected} | Still healthy: {people.Count - countInfected}");
                }

                Thread.Sleep(50);
            }
        }
    }
}
