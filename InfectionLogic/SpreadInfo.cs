﻿// <copyright file="SpreadInfo.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace DB
{
    /// <summary>
    /// Data class for storing information about the spread of a virus / disease.
    /// </summary>
    public class SpreadInfo
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SpreadInfo"/> class.
        /// </summary>
        /// <param name="sourceID">The ID of the person who infected someone.</param>
        /// <param name="targetID">The ID of the person who  was infected by someone.</param>
        /// <param name="x">The horizontal coordinate of the location of the infection.</param>
        /// <param name="y">The vertical coordinate of the location of the infection.</param>
        public SpreadInfo(int sourceID, int targetID, int x, int y)
        {
            this.SourceID = sourceID;
            this.TargetID = targetID;
            this.X = x;
            this.Y = y;
        }

        /// <summary>
        /// Gets or sets the ID of the person who infected someone.
        /// </summary>
        public int SourceID { get; set; }

        /// <summary>
        /// Gets or sets the ID of the person who was infected by someone.
        /// </summary>
        public int TargetID { get; set; }

        /// <summary>
        /// Gets or sets the horizontal coordinate of the location of the infection.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or sets the vertical coordinate of the location of the infection.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Gets or Sets the PK.
        /// </summary>
        public int DummyKey { get; set; }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"{this.SourceID} --> {this.TargetID}  (X= {this.X}, Y= {this.Y})";
        }
    }
}
