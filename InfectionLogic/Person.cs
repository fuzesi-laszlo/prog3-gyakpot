﻿// <copyright file="Person.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace InfectionLogic
{
    using System;

    /// <summary>
    /// Represents a person who can be infected, can wear a mask, and has 2D location and ID.
    /// </summary>
    public class Person
    {
        private static int counter;

        /// <summary>
        /// Initializes a new instance of the <see cref="Person"/> class with a(n) <see cref="ID"/> (if <paramref name="assignID"/> <c>= true</c>) .
        /// </summary>
        /// <param name="assignID">If <c>true</c>, the initialised object gets an ID.</param>
        public Person(bool assignID)
        {
            if (assignID)
            {
                this.ID = ++counter;
            }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Person"/> class with an auto-incremented <see cref="ID"/>.
        /// </summary>
        /// <param name="x">Location on the horizontal axis. (0: left edge).</param>
        /// <param name="y">Location on the vertical axis. (0: top).</param>
        /// <param name="isInfected">Assigns a <see cref="bool"/> to <see cref="IsInfected"/>.</param>
        /// <param name="isMasked">Assigns a <see cref="bool"/> to <see cref="IsMasked"/>.</param>
        public Person(int x, int y, bool isInfected, bool isMasked)
        {
            this.ID = ++counter;
            this.X = x;
            this.Y = y;
            this.IsInfected = isInfected;
            this.IsMasked = isMasked;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Person"/> class.
        /// </summary>
        public Person()
        {
        }

        /// <summary>
        /// Gets or Sets the ID number.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or Sets the horizontal coordinate.
        /// </summary>
        public int X { get; set; }

        /// <summary>
        /// Gets or Sets the vertical coordinate.
        /// </summary>
        public int Y { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the person is infected or not.
        /// </summary>
        public bool IsInfected { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the person wears a mask or not.
        /// </summary>
        public bool IsMasked { get; set; }

        /// <summary>
        /// Resets <see cref="counter"/> to 0.
        /// </summary>
        public static void ResetCounter() => Person.counter = 0;

        /// <summary>
        /// Interprets the received (dx, dy) values as a movement vector to shift the location of the person without leaving the specified field.
        /// (be careful that it should not leave the console, the console sizes are also specified in the parameters).
        /// </summary>
        /// <param name="dx">Horizontal vetor of motion.</param>
        /// <param name="dy">Vertical vetor of motion.</param>
        /// <param name="width">Maximum width of the field.</param>
        /// <param name="height">Maximum height of the field.</param>
        public void MovePerson(int dx, int dy, int width, int height)
        {
            if (dx == 0 || dy == 0)
            {
                return;
            }

            this.X += dx % width;
            this.Y += dy % height;

            if (this.X >= width)
            {
                this.X -= width;
            }
            else if (this.X < 0)
            {
                this.X += width;
            }

            if (this.Y >= height)
            {
                this.Y -= height;
            }
            else if (this.Y < 0)
            {
                this.Y += height;
            }
        }

        /// <summary>
        /// Ha bármelyik személy esetén (a paraméterként kapott <paramref name="otherPerson"/> vagy az aktuális példány)
        /// <see cref="IsInfected"/> <c>= true</c>, &amp; <see cref="IsMasked"/> <c>= false</c>, &amp;
        /// a távolságuk (<see cref="X"/> , <see cref="Y"/>) max. 2,
        /// akkor mindkét személy fertőzött lesz, és a metódus visszatérési értéke <c>: true</c>.
        /// </summary>
        /// <param name="otherPerson">A másik személy.</param>
        /// <returns>A <see cref="bool"/> value of whether there was an interaction between the two persons or not.</returns>
        public bool InteractWith(Person otherPerson)
        {
            if (otherPerson is null)
            {
                throw new ArgumentNullException(nameof(otherPerson), " was NULL");
            }

            double distance = Math.Sqrt(Math.Pow(this.X - otherPerson.X, 2) + Math.Pow(this.Y - otherPerson.Y, 2));
            if (!this.Equals(otherPerson) &&
                (this.IsInfected || otherPerson.IsInfected) // either is infected
                && !(this.IsMasked && otherPerson.IsMasked) // NOT BOTH masked
                /* && Math.Abs(this.X - otherPerson.X) < 3 // horizontal distance is less than 3 */
                /* && Math.Abs(this.Y - otherPerson.Y) < 3) // vertical distance is less than 3 */
                && distance < 3)
            {
                this.IsInfected = true;
                otherPerson.IsInfected = true;
                return true;
            }

            return false;
        }

        /// <inheritdoc/>
        public override string ToString()
        {
            return $"#{this.ID} (X= {this.X}, Y= {this.Y}) | "
                + (this.IsInfected ? "INFECTED" : "NOT infected") + " | "
                + (this.IsMasked ? "Masked" : "NO mask");
        }

        /// <inheritdoc/>
        public override bool Equals(object obj)
        {
            return obj is Person other
                && this.ID == other.ID
                && this.X == other.X
                && this.Y == other.Y
                && this.IsInfected == other.IsInfected
                && this.IsMasked == other.IsMasked;
        }

        /// <inheritdoc/>
        public override int GetHashCode()
        {
            return this.ID + this.X + this.Y;
        }
    }
}
