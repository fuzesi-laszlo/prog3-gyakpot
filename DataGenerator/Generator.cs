﻿// <copyright file="Generator.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

[assembly: System.CLSCompliant(false)]

namespace DataGenerator
{
    using System;
    using System.Xml.Linq;

    /// <summary>
    /// Generates outbeak data.
    /// </summary>
    public static class Generator
    {
        private static readonly Random Rnd = new();

        /// <summary>
        /// Returns an XML document of outbreak data.
        /// </summary>
        /// <param name="neptun">Secret code.</param>
        /// <param name="num">Number of outbreaks.</param>
        /// <param name="width">Maximum horizontal coordinate of an outbreak.</param>
        /// <param name="height">Maximum vertical coordinate of an outbreak.</param>
        /// <returns>The XML document containing <paramref name="num"/> number of outbreeaks between (0,0) and (<paramref name="width"/>,<paramref name="height"/>).</returns>
        public static XDocument GenerateData(string neptun, int num, int width, int height)
        {
            XDocument output = new(new XElement(neptun));
            for (int i = 0; i < num; i++)
            {
                XElement elem = new("outbreak",
                    new XElement("location", new XAttribute("x", Rnd.Next(width)), new XAttribute("y", Rnd.Next(height))),
                    new XElement("strength", Rnd.Next(9) + 1));
                output.Root.Add(elem);
            }

            return output;
        }
    }
}
